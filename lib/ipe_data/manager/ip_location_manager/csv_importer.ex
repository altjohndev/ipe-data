defmodule IpeData.IPLocationManager.CSVImporter do
  @moduledoc false

  alias Ecto.Changeset
  alias IpeData.DownloadUtils
  alias IpeData.IPLocation
  alias IpeData.IPLocationManager
  alias IpeData.RepoUtils

  require Logger

  @import_from_csv_settings %{
    remote_csv_url: nil,
    read_ahead: 50_000,
    downloader: DownloadUtils,
    insert_all: []
  }

  @spec import_data(IPLocationManager.import_from_csv_options()) ::
          {:ok, IPLocationManager.import_from_csv_metadata()}
          | {:error, IPLocationManager.import_from_csv_error()}
  def import_data(options) do
    started_at = NaiveDateTime.utc_now()

    %{
      remote_csv_url: url,
      local_csv_path: path,
      read_ahead: read_ahead,
      downloader: downloader,
      insert_all: insert_all_options
    } =
      @import_from_csv_settings
      |> Map.merge(Map.new(options))
      |> Map.put_new_lazy(:local_csv_path, &default_local_csv_path/0)

    with(
      {:ok, downloaded_at} <- maybe_download(url, path, downloader),
      {:ok, stream} <- stream_csv_file(path, read_ahead)
    ) do
      %{
        inserted_amount: inserted_amount,
        discarded_amount: discarded_amount
      } = stream |> NimbleCSV.RFC4180.parse_stream() |> process_stream(insert_all_options)

      metadata = %{
        started_at: started_at,
        downloaded_at: downloaded_at,
        finished_at: NaiveDateTime.utc_now(),
        inserted_amount: inserted_amount,
        discarded_amount: discarded_amount
      }

      {:ok, metadata}
    end
  end

  defp default_local_csv_path do
    Path.join([:code.priv_dir(:ipe_data), "repo", "seeds", "ip_locations.csv"])
  end

  defp maybe_download(url, path, downloader) do
    if url do
      case downloader.download(url, path) do
        :ok -> {:ok, NaiveDateTime.utc_now()}
        :error -> {:error, :failed_to_download}
      end
    else
      {:ok, nil}
    end
  end

  defp stream_csv_file(path, read_ahead) do
    if File.exists?(path) and not File.dir?(path) do
      {:ok, File.stream!(path, read_ahead: read_ahead)}
    else
      Logger.error("defined path does not have a file", path: path)
      {:error, :failed_to_read_csv}
    end
  end

  @process_data %{total_amount: 0, inserted_amount: 0, batch_size: 0, batch: []}

  defp process_stream(stream, insert_all_options) do
    %{
      total_amount: total_amount,
      inserted_amount: inserted_amount,
      batch_size: batch_size,
      batch: batch
    } = Enum.reduce(stream, @process_data, &process_row(&1, &2, insert_all_options))

    inserted_amount =
      if batch_size > 0 do
        inserted_amount + insert_batch(batch, insert_all_options)
      else
        inserted_amount
      end

    %{inserted_amount: inserted_amount, discarded_amount: total_amount - inserted_amount}
  end

  @record_fields [:ip_address, :country_code, :country, :city, :latitude, :longitude]
  @batch_maximum_parameters 65_535
  @batch_size_limit div(@batch_maximum_parameters, Enum.count(@record_fields))

  defp process_row(row, data, insert_all_options) do
    data = %{data | total_amount: data.total_amount + 1}

    case parse_row(row) do
      {:ok, entry} ->
        batch_size = data.batch_size + 1
        batch = [entry | data.batch]

        if batch_size < @batch_size_limit do
          %{data | batch_size: batch_size, batch: batch}
        else
          %{
            data
            | inserted_amount: data.inserted_amount + insert_batch(batch, insert_all_options),
              batch_size: 0,
              batch: []
          }
        end

      _error ->
        data
    end
  end

  defp parse_row(row) do
    case row do
      [ip_address, country_code, country, city, latitude, longitude | _columns] ->
        params = %{
          ip_address: ip_address,
          country_code: country_code,
          country: country,
          city: city,
          latitude: latitude,
          longitude: longitude
        }

        case params |> IPLocation.create_changeset() |> Changeset.apply_action(:validate) do
          {:ok, %IPLocation{} = ip_location} -> {:ok, Map.take(ip_location, @record_fields)}
          {:error, %Changeset{}} -> :error
        end

      _row ->
        :error
    end
  end

  defp insert_batch(batch, insert_all_options) do
    RepoUtils.insert_all(
      IPLocation,
      Enum.uniq_by(batch, & &1.ip_address),
      Keyword.merge(insert_all_options, on_conflict: :nothing, conflict_target: :ip_address)
    )
  end
end
