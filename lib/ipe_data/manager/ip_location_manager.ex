defmodule IpeData.IPLocationManager do
  @moduledoc """
  Repository operations that can insert, update, or delete `IpeData.IPLocation` records.
  """

  alias IpeData.IPLocationManager.CSVImporter

  @type import_from_csv_options :: [
          local_csv_path: Path.t(),
          remote_csv_url: String.t(),
          read_ahead: pos_integer,
          downloader: module,
          insert_all: RepoUtils.options()
        ]

  @type import_from_csv_metadata :: %{
          started_at: NaiveDateTime.t(),
          downloaded_at: NaiveDateTime.t() | nil,
          finished_at: NaiveDateTime.t(),
          accepted_rows: non_neg_integer,
          discarded_rows: non_neg_integer,
          inserted_records: non_neg_integer
        }

  @type import_from_csv_error :: :failed_to_download | :failed_to_read_csv

  @doc """
  Seeds `IpeData.IPLocation` data through CSV file.

  If `:remote_csv_url` option is set, it will download the CSV from the external source.

  Returns `{:ok, metadata}` on success or `{:error, reason}` on failure.

  ## Options

  - `:local_csv_path` - The local path for the "CSV" file. Defaults to `<priv_dir>/repo/seeds/ip_locations.csv`

  - `:remote_csv_url` - The remote URL to download the "CSV" file. If defined, it will download and save in path
    defined by `:local_csv_path` option

  - `:downloader` - Module with function `download` that receives `url` and `path` as arguments and returns `:ok` on
    success or `:error` on failure. Useful for mocks during unit testing

  - `:insert_all` - A keyword list that can have `:repo` option and `c:Ecto.Repo.insert_all/3` related options
  """
  @doc since: "0.0.1"
  @spec import_from_csv(import_from_csv_options) :: {:ok, import_from_csv_metadata} | {:error, import_from_csv_error}
  defdelegate import_from_csv(options \\ []), to: CSVImporter, as: :import_data
end
