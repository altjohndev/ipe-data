defmodule IpeData.Migrator do
  @moduledoc """
  Migration operations related to `IpeData` entities.
  """

  alias Ecto.Migrator

  @doc """
  Performs pending migrations.
  """
  @doc since: "0.0.1"
  @spec migrate(module) :: :ok
  def migrate(repo) do
    {:ok, _function, _apps} = Migrator.with_repo(repo, &Migrator.run(&1, paths(), :up, all: true))
    :ok
  end

  defp paths, do: [Path.join([:code.priv_dir(:ipe_data), "repo", "migrations"])]

  @doc """
  Rollbacks to version provided.
  """
  @doc since: "0.0.1"
  @spec rollback(module, String.t() | non_neg_integer) :: :ok
  def rollback(repo, version) do
    {:ok, _function, _apps} = Migrator.with_repo(repo, &Migrator.run(&1, paths(), :down, to: version))
    :ok
  end
end
