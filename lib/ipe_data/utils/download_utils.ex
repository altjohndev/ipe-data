defmodule IpeData.DownloadUtils do
  @moduledoc false

  require Logger

  @doc """
  Downloads file from url into local path.

  Returns `:ok` on success or `:error` on failure.
  """
  @spec download(url :: String.t(), path :: String.t()) :: :ok | :error
  def download(url, path) do
    path |> Path.dirname() |> File.mkdir_p()

    case :httpc.request(:get, {url, []}, [], stream: String.to_charlist(path)) do
      {:ok, _result} -> :ok
      _result -> :error
    end
  end
end
