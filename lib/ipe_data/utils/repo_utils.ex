defmodule IpeData.RepoUtils do
  @moduledoc false

  @type clauses :: Keyword.t() | map
  @type entries_or_query :: Ecto.Query.t() | [Keyword.t() | map]
  @type options :: [{:repo, module} | {atom, any}]
  @type queryable :: Ecto.Queryable.t()
  @type schema_or_source :: module | String.t() | {String.t(), module}
  @type struct_or_changeset :: Ecto.Schema.t() | Ecto.Changeset.t()

  @type fetch_error :: :not_found

  @doc """
  Fetches an existing record from repository.

  If `:repo` option is not set, it fetches through `fetch_default_read_repo!/0`.

  Similar to `c:Ecto.Repo.get_by/3`, but returns `{:ok, schema}` on success or `{:error, reason}` on failure.
  """
  @spec fetch_by(queryable, clauses, options) :: {:ok, any} | {:error, fetch_error}
  def fetch_by(queryable, clauses, options) do
    {repo, options} = Keyword.pop_lazy(options, :repo, &fetch_default_read_repo!/0)

    case repo.get_by(queryable, clauses, options) do
      nil -> {:error, :not_found}
      result -> {:ok, result}
    end
  end

  @doc """
  Fetches default repository  module for read-only operations.

  It fetches through the `:default_read_repo` application environment, falling back to `:default_repo` and
  `:default_write_repo`.

  Raises on failure.
  """
  @spec fetch_default_read_repo! :: module
  def fetch_default_read_repo!, do: at_least_one_env(~w[default_read_repo default_repo default_write_repo]a)

  defp at_least_one_env([key]), do: Application.fetch_env!(:ipe_data, key)
  defp at_least_one_env([key | keys]), do: Application.get_env(:ipe_data, key) || at_least_one_env(keys)

  @doc """
  Fetches default repository module for write-only operations.

  It fetches through the `:default_write_repo` application environment, falling back to `:default_repo`.

  Raises on failure.
  """
  @spec fetch_default_write_repo! :: module
  def fetch_default_write_repo!, do: at_least_one_env(~w[default_write_repo default_repo]a)

  @doc """
  Inserts a struct defined via `Ecto.Schema` or a `Ecto.Changeset`.

  If `:repo` option is not set, it fetches through `fetch_default_write_repo!/0`.

  Similar to `c:Ecto.Repo.insert!/2`.
  """
  @spec insert!(struct_or_changeset, options) :: Ecto.Schema.t()
  def insert!(struct_or_changeset, options) do
    {repo, options} = Keyword.pop_lazy(options, :repo, &fetch_default_write_repo!/0)
    repo.insert!(struct_or_changeset, options)
  end

  @doc """
  Inserts all entries into the repository.

  If `:repo` option is not set, it fetches through `fetch_default_write_repo!/0`.

  Similar to `c:Ecto.Repo.insert_all/3`, but returns only the amount value.
  """
  @spec insert_all(schema_or_source, entries_or_query, options) :: non_neg_integer
  def insert_all(schema_or_source, entries_or_query, options) do
    {repo, options} = Keyword.pop_lazy(options, :repo, &fetch_default_write_repo!/0)
    {amount, _returning_data} = repo.insert_all(schema_or_source, entries_or_query, options)
    amount
  end
end
