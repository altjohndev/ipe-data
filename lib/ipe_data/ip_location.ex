defmodule IpeData.IPLocation do
  @moduledoc """
  Schema related to the latest geo location data for a particular IPv4 address.

  ## Fields

  - `:ip_address` - A valid "IPv4" address. It is required and it must be unique

  - `:country_code` - Any string in the "ISO 3166 alpha-2" country code format. It is required. It does not validate if
    it is a valid ISO 3166 alpha-2 code

  - `:country` - The country name. It is required. It does not validate if the country exists or if it matches the
    `:country_code`

  - `:city` - The country name. It is required. It does not validate if the city exists or if the city belongs to the
    `:country`

  - `:latitude` - The latitude decimal value. It is required. Values are sanitized to fit the range of `+-90`

  - `:longitude` - The longitude decimal value. It is required. Values are sanitized to fit the range of `+-180`
  """

  @behaviour Access

  use Ecto.Schema

  alias __MODULE__
  alias Ecto.Changeset

  @derive {Jason.Encoder, except: [:__meta__]}

  @primary_key {:ip_address, :string, []}

  schema "ip_locations" do
    field :country_code, :string
    field :country, :string
    field :city, :string
    field :latitude, :decimal
    field :longitude, :decimal
  end

  @type t :: %IPLocation{
          ip_address: String.t(),
          country_code: String.t(),
          country: String.t(),
          city: String.t(),
          latitude: Decimal.t(),
          longitude: Decimal.t()
        }

  @cast_for_create [:ip_address, :country_code, :country, :city, :latitude, :longitude]
  @required_for_create [:ip_address, :country_code, :country, :city, :latitude, :longitude]

  @ipv4_regex ~r/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
  @country_code_regex ~r/^[A-Z][A-Z]$/

  @doc """
  `Ecto.Changeset` for create operations.
  """
  @doc since: "0.0.1"
  @spec create_changeset(map) :: Changeset.t()
  def create_changeset(params) do
    %IPLocation{}
    |> Changeset.cast(params, @cast_for_create)
    |> Changeset.validate_required(@required_for_create)
    |> Changeset.update_change(:ip_address, &sanitize_ip_address/1)
    |> Changeset.validate_format(:ip_address, @ipv4_regex)
    |> Changeset.update_change(:country_code, &sanitize_country_code/1)
    |> Changeset.validate_format(:country_code, @country_code_regex)
    |> Changeset.update_change(:country, &sanitize_country/1)
    |> Changeset.validate_length(:country, min: 1)
    |> Changeset.update_change(:city, &sanitize_city/1)
    |> Changeset.validate_length(:city, min: 1)
    |> Changeset.update_change(:latitude, &sanitize_latitude/1)
    |> Changeset.update_change(:longitude, &sanitize_longitude/1)
  end

  defp sanitize_ip_address(value), do: String.trim(value)
  defp sanitize_country_code(value), do: value |> String.trim() |> String.upcase()
  defp sanitize_country(value), do: sanitize_string(value)
  defp sanitize_city(value), do: sanitize_string(value)
  defp sanitize_latitude(%Decimal{} = value), do: ensure_modular_boundary(value, 90)
  defp sanitize_longitude(%Decimal{} = value), do: ensure_modular_boundary(value, 180)

  defp sanitize_string(value) do
    value
    |> String.trim()
    |> String.split(" ", trim: true)
    |> Enum.map_join(" ", &String.trim/1)
  end

  defp ensure_modular_boundary(%Decimal{} = value, modular_value) do
    multiplier = value |> Decimal.abs() |> Decimal.div_int(modular_value)
    new_value = Decimal.add(value, multiplier |> Decimal.mult(modular_value) |> Decimal.mult(-value.sign))

    if multiplier |> Decimal.rem(2) |> Decimal.equal?(0) do
      new_value
    else
      Decimal.add(new_value, Decimal.mult(modular_value, -value.sign))
    end
  end

  @doc false
  @impl Access
  @spec get_and_update(IPLocation.t(), atom, (any -> :pop | {any, any})) :: {any, IPLocation.t()}
  def get_and_update(ip_location, key, fun) do
    value = Map.get(ip_location, key)

    {result_value, value_to_update} =
      case fun.(value) do
        :pop -> {value, nil}
        {_result_value, _value_to_update} = result -> result
      end

    if key in [:__struct__, :__meta__] do
      {result_value, ip_location}
    else
      {result_value, struct(ip_location, [{key, value_to_update}])}
    end
  end

  @doc false
  @impl Access
  @spec fetch(IPLocation.t(), atom) :: {:ok, any} | :error
  defdelegate fetch(ip_location, key), to: Map

  @doc false
  @impl Access
  @spec pop(IPLocation.t(), atom) :: {any, IPLocation.t()}
  def pop(ip_location, key), do: get_and_update(ip_location, key, fn _value -> :pop end)
end
