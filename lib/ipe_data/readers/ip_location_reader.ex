defmodule IpeData.IPLocationReader do
  @moduledoc """
  Read-only repository operations related to `IpeData.IPLocation`.
  """

  alias IpeData.IPLocation
  alias IpeData.RepoUtils

  @doc """
  Fetches an existing `IpeData.IPLocation` record from repository.
  """
  @doc since: "0.0.1"
  @spec fetch(String.t(), Keyword.t()) :: {:ok, IPLocation.t()} | {:error, :not_found}
  def fetch(ip_address, options \\ []), do: RepoUtils.fetch_by(IPLocation, [ip_address: ip_address], options)
end
