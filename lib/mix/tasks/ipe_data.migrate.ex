defmodule Mix.Tasks.IpeData.Migrate do
  @moduledoc """
  Mix task to migrate `IpeData` entities.

  ## Usage

  ```
  mix ipe_data.migrate MyProject.Repo
  ```
  """

  use Mix.Task

  alias IpeData.Migrator

  @requirements ["app.config"]

  @doc since: "0.0.1"
  @spec run(list(String.t())) :: any
  def run([repo]) do
    {:ok, _apps} = Application.ensure_all_started(:ecto_sql)

    [repo]
    |> Module.concat()
    |> Migrator.migrate()
  end
end
