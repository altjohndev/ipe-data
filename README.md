[![pipeline status](https://gitlab.com/altjohndev/ipe-data/badges/main/pipeline.svg)](https://gitlab.com/altjohndev/ipe-data/-/commits/main)
[![coverage report](https://gitlab.com/altjohndev/ipe-data/badges/main/coverage.svg)](https://gitlab.com/altjohndev/ipe-data/-/commits/main)

# Ipê Data

Data management library for [Ipê](https://gitlab.com/altjohndev/ipe) services.

## Installation and Usage

Add `:ipe_data` as a dependency to your project's `mix.exs`:

```elixir
defp deps do
  [
    {:ipe_data, git: "https://gitlab.com/altjohndev/ipe-data.git", tag: "v0.0.1"}
  ]
end
```

Some utility modules for tests (such as `IpeData.Test.Fake`) are available through `:test` mix environment. To use in
your project, add `:env` to the dependency:

```elixir
defp deps do
  [
    {:ipe_data, git: "https://gitlab.com/altjohndev/ipe-data.git", tag: "v0.0.1", env: Mix.env()}
  ]
end
```

Create a repository for your project:

```elixir
defmodule MyProject.Repo do
  use Ecto.Repo, otp_app: :my_project, adapter: Ecto.Adapters.Postgres
end
```

Set it as default repo through configuration:

```elixir
config :ipe_data, default_repo: MyProject.Repo
```

Migrate locally through mix task:

```shell
mix ipe_data.migrate MyProject.Repo
```

In a release environment (without `mix`), use:

```elixir
IpeData.Migrator.migrate(MyProject.Repo)
```

Finally, you can use any reader module to fetch data:

```elixir
IpeData.IPLocationReader.fetch("10.0.0.1")
```

And any manager module to insert, update, or remove data:

```elixir
IpeData.IPLocationManager.import_from_csv(local_csv_path: "my/custom/path/to/ip_locations.csv")
```

## Configuration

You can configure any of the following:

```elixir
config :ipe_data,
  default_repo: MyProject.PrimaryRepo,
  default_read_repo: MyProject.ReplicaRepo,
  default_write_repo: MyProject.PrimaryRepo
```

The `:default_read_repo` configuration will be used for any read operation that did not received `:repo` as option.

The `:default_write_repo` configuration will be used for any read or write operation that did not received `:repo` as
option. For read operations, it will be used only if both `:default_read_repo` and `:default_repo` were not set.

The `:default_repo` configuration will be used for any operation that did not received `:repo` as option. For read
operations, it will be used only if `:default_read_repo` was not set. For write operations, it will be used only
if `:default_write_repo` was not set.

## IP locations

`IpeData.IPLocation` defines the schema for the latest geo location data for a particular IPv4 address.

Read-only operations are available in `IpeData.IPLocationReader`.

Insert, update, or delete operations are available in `IpeData.IPLocationManager`.

Records can be seeded through `IpeData.IPLocationManager.import_from_csv/1`.
