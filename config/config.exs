import Config

config :ipe_data, default_repo: IpeData.Test.Repo, ecto_repos: [IpeData.Test.Repo]

config :ipe_data, IpeData.Test.Repo,
  pool: Ecto.Adapters.SQL.Sandbox,
  url: System.get_env("IPE_DATA_REPO_URL", "ecto://postgres:postgres@localhost:5432/ipe_data_test"),
  log: false
