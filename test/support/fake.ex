defmodule IpeData.Test.Fake do
  @moduledoc """
  Utility functions to generate fake data
  """

  alias IpeData.IPLocation
  alias IpeData.RepoUtils

  @spec create(module, Keyword.t() | map) :: Ecto.Schema.t()
  def create(schema, params \\ %{}) do
    schema
    |> schema_params(params)
    |> schema.create_changeset()
    |> RepoUtils.insert!([])
  end

  @spec city_name :: String.t()
  def city_name, do: upcased_letters(10)

  @spec country_code :: String.t()
  def country_code, do: upcased_letters(2)

  @spec country_name :: String.t()
  def country_name, do: upcased_letters(10)

  @spec decimal(pos_integer) :: Decimal.t()
  def decimal(threshold) do
    :rand.uniform()
    |> Decimal.from_float()
    |> Decimal.add(Enum.random(0..threshold))
    |> Decimal.mult(Enum.random([1, -1]))
  end

  @spec ipv4 :: String.t()
  def ipv4 do
    "#{Enum.random(0..255)}.#{Enum.random(0..255)}.#{Enum.random(0..255)}.#{Enum.random(0..255)}"
  end

  @spec latitude :: Decimal.t()
  def latitude, do: decimal(89)

  @spec longitude :: Decimal.t()
  def longitude, do: decimal(179)

  @spec schema_params(module) :: map
  def schema_params(IPLocation) do
    %{
      ip_address: ipv4(),
      country_code: country_code(),
      country: country_name(),
      city: city_name(),
      latitude: latitude(),
      longitude: longitude()
    }
  end

  @spec schema_params(module, Keyword.t() | map) :: map
  def schema_params(schema, params), do: Map.merge(schema_params(schema), Map.new(params))

  @spec upcased_letter :: String.t()
  def upcased_letter, do: ?A..?Z |> Enum.random() |> List.wrap() |> List.to_string()

  @spec upcased_letters(pos_integer) :: String.t()
  def upcased_letters(length), do: Enum.map_join(1..length, fn _index -> upcased_letter() end)
end
