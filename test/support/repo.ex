defmodule IpeData.Test.Repo do
  @moduledoc """
  `Ecto.Repo` for testing.
  """

  use Ecto.Repo, otp_app: :ipe_data, adapter: Ecto.Adapters.Postgres
end
