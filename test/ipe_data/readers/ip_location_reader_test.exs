defmodule IpeData.IPLocationReaderTest do
  @moduledoc """
  Test cases related to `IpeData.IPLocationReader`
  """

  use ExUnit.Case, async: true

  alias Ecto.Adapters.SQL.Sandbox
  alias IpeData.IPLocation
  alias IpeData.IPLocationReader
  alias IpeData.Test.Fake
  alias IpeData.Test.Repo

  setup do
    Sandbox.checkout(Repo)
  end

  describe "fetch/2" do
    test "fetches existing record by ip_address" do
      %IPLocation{} = ip_location = Fake.create(IPLocation)
      assert {:ok, ^ip_location} = IPLocationReader.fetch(ip_location.ip_address)
    end

    test "fails if no record does not exist" do
      ip_address = Fake.ipv4()
      assert {:error, :not_found} = IPLocationReader.fetch(ip_address)
    end
  end
end
