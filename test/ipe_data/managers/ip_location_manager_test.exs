defmodule IpeData.IPLocationManagerTest do
  @moduledoc """
  Test cases related to `IpeData.IPLocationManager`
  """

  use ExUnit.Case, async: true

  import Ecto.Query, only: [from: 2]

  alias Ecto.Adapters.SQL.Sandbox
  alias IpeData.IPLocation
  alias IpeData.IPLocationManager
  alias IpeData.Test.Repo

  @headers ["ip_address", "country_code", "country", "city", "latitude", "longitude"]

  @assignment_raw_url "https://raw.githubusercontent.com/viodotcom/backend-assignment-elixir/master/"

  @cloud_data_dump_url Path.join(@assignment_raw_url, "cloud_data_dump.csv")
  @data_dump_url Path.join(@assignment_raw_url, "data_dump.csv")

  describe "import_from_csv/1" do
    @tag :tmp_dir
    test "imports local file", context do
      Sandbox.checkout(Repo)

      local_csv_path =
        write_csv_data(
          context,
          for first <- 1..127, second <- 1..43, third <- 1..2 do
            ["#{first}.#{second}.#{third}.0", "BR", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075"]
          end
        )

      assert {:ok, metadata} = IPLocationManager.import_from_csv(local_csv_path: local_csv_path)

      assert_local_metadata(metadata, 10_922, 0)
      assert Repo.aggregate(IPLocation, :count) == 10_922
    end

    @tag :tmp_dir
    @tag :integration
    @tag :cloud_data_dump
    test "imports from #{@cloud_data_dump_url}", context do
      Sandbox.checkout(Repo)

      options = [
        local_csv_path: Path.join(context.tmp_dir, "cloud_data_dump.csv"),
        remote_csv_url: @cloud_data_dump_url
      ]

      assert {:ok, metadata} = IPLocationManager.import_from_csv(options)

      assert_remote_metadata(metadata, 17_015, 2_984)
      assert Repo.aggregate(IPLocation, :count) == 17_015
    end

    @tag :tmp_dir
    @tag :integration
    @tag :data_dump
    test "imports from #{@data_dump_url}", context do
      Sandbox.checkout(Repo)

      options = [local_csv_path: Path.join(context.tmp_dir, "data_dump.csv"), remote_csv_url: @data_dump_url]
      assert {:ok, metadata} = IPLocationManager.import_from_csv(options)

      assert_remote_metadata(metadata, 849_275, 150_725)
      assert Repo.aggregate(IPLocation, :count) == 849_275
    end

    @tag :tmp_dir
    test "imports from remote (mocked)", context do
      Sandbox.checkout(Repo)

      local_csv_path =
        write_csv_data(context, [
          ["111.111.111.110", "BR", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["111.111.111.111", "NL", "Netherlands", "Amsterdam", "52.37403", "4.88969"],
          ["111.111.111.112", "EE", "Estonia", "Tallinn", "59.43696", "24.75353"]
        ])

      options = [local_csv_path: local_csv_path, remote_csv_url: "https://ok", downloader: __MODULE__]

      assert {:ok, metadata} = IPLocationManager.import_from_csv(options)

      latitude1 = Decimal.new("-22.90278")
      longitude1 = Decimal.new("-43.2075")

      latitude2 = Decimal.new("52.37403")
      longitude2 = Decimal.new("4.88969")

      latitude3 = Decimal.new("59.43696")
      longitude3 = Decimal.new("24.75353")

      assert [
               %IPLocation{
                 ip_address: "111.111.111.110",
                 country_code: "BR",
                 country: "Brazil",
                 city: "Rio de Janeiro",
                 latitude: ^latitude1,
                 longitude: ^longitude1
               },
               %IPLocation{
                 ip_address: "111.111.111.111",
                 country_code: "NL",
                 country: "Netherlands",
                 city: "Amsterdam",
                 latitude: ^latitude2,
                 longitude: ^longitude2
               },
               %IPLocation{
                 ip_address: "111.111.111.112",
                 country_code: "EE",
                 country: "Estonia",
                 city: "Tallinn",
                 latitude: ^latitude3,
                 longitude: ^longitude3
               }
             ] = Repo.all(from IPLocation, order_by: [asc: :ip_address])

      assert_remote_metadata(metadata, 3, 0)
    end

    @tag :tmp_dir
    test "imports local file, trimming string cells", context do
      Sandbox.checkout(Repo)

      local_csv_path =
        write_csv_data(context, [
          ["  111.111.111.110", " BR ", " Brazil ", "Rio    de    Janeiro", "-22.90278", "-43.2075"],
          ["111.111.111.111  ", " NL", "Netherlands ", "    Amsterdam", "52.37403", "4.88969"],
          ["  111.111.111.112  ", "EE ", " Estonia ", "Tallinn   ", "59.43696", "24.75353"]
        ])

      assert {:ok, metadata} = IPLocationManager.import_from_csv(local_csv_path: local_csv_path)

      latitude1 = Decimal.new("-22.90278")
      longitude1 = Decimal.new("-43.2075")

      latitude2 = Decimal.new("52.37403")
      longitude2 = Decimal.new("4.88969")

      latitude3 = Decimal.new("59.43696")
      longitude3 = Decimal.new("24.75353")

      assert [
               %IPLocation{
                 ip_address: "111.111.111.110",
                 country_code: "BR",
                 country: "Brazil",
                 city: "Rio de Janeiro",
                 latitude: ^latitude1,
                 longitude: ^longitude1
               },
               %IPLocation{
                 ip_address: "111.111.111.111",
                 country_code: "NL",
                 country: "Netherlands",
                 city: "Amsterdam",
                 latitude: ^latitude2,
                 longitude: ^longitude2
               },
               %IPLocation{
                 ip_address: "111.111.111.112",
                 country_code: "EE",
                 country: "Estonia",
                 city: "Tallinn",
                 latitude: ^latitude3,
                 longitude: ^longitude3
               }
             ] = Repo.all(from IPLocation, order_by: [asc: :ip_address])

      assert_local_metadata(metadata, 3, 0)
    end

    @tag :tmp_dir
    test "imports local file, adjusting latitude and longitude modular boundaries", context do
      Sandbox.checkout(Repo)

      local_csv_path =
        write_csv_data(context, [
          ["111.111.111.110", "BR", "Brazil", "Rio de Janeiro", "-202.90278", "-403.2075"],
          ["111.111.111.111", "NL", "Netherlands", "Amsterdam", "232.37403", "364.88969"],
          ["111.111.111.112", "EE", "Estonia", "Tallinn", "-120.56304", "-335.24647"]
        ])

      assert {:ok, metadata} = IPLocationManager.import_from_csv(local_csv_path: local_csv_path)

      latitude1 = Decimal.new("-22.90278")
      longitude1 = Decimal.new("-43.2075")

      latitude2 = Decimal.new("52.37403")
      longitude2 = Decimal.new("4.88969")

      latitude3 = Decimal.new("59.43696")
      longitude3 = Decimal.new("24.75353")

      assert [
               %IPLocation{
                 ip_address: "111.111.111.110",
                 country_code: "BR",
                 country: "Brazil",
                 city: "Rio de Janeiro",
                 latitude: ^latitude1,
                 longitude: ^longitude1
               },
               %IPLocation{
                 ip_address: "111.111.111.111",
                 country_code: "NL",
                 country: "Netherlands",
                 city: "Amsterdam",
                 latitude: ^latitude2,
                 longitude: ^longitude2
               },
               %IPLocation{
                 ip_address: "111.111.111.112",
                 country_code: "EE",
                 country: "Estonia",
                 city: "Tallinn",
                 latitude: ^latitude3,
                 longitude: ^longitude3
               }
             ] = Repo.all(from IPLocation, order_by: [asc: :ip_address])

      assert_local_metadata(metadata, 3, 0)
    end

    @tag :tmp_dir
    test "imports local file, ignoring additional columns", context do
      Sandbox.checkout(Repo)

      local_csv_path =
        write_csv_data(context, Enum.concat(@headers, ~w[greeting target]), [
          ["111.111.111.110", "BR", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075", "hey", "you"],
          ["111.111.111.111", "NL", "Netherlands", "Amsterdam", "52.37403", "4.88969", "hey", "Jude"],
          ["111.111.111.112", "EE", "Estonia", "Tallinn", "59.43696", "24.75353", "hey", "ho"]
        ])

      assert {:ok, metadata} = IPLocationManager.import_from_csv(local_csv_path: local_csv_path)

      latitude1 = Decimal.new("-22.90278")
      longitude1 = Decimal.new("-43.2075")

      latitude2 = Decimal.new("52.37403")
      longitude2 = Decimal.new("4.88969")

      latitude3 = Decimal.new("59.43696")
      longitude3 = Decimal.new("24.75353")

      assert [
               %IPLocation{
                 ip_address: "111.111.111.110",
                 country_code: "BR",
                 country: "Brazil",
                 city: "Rio de Janeiro",
                 latitude: ^latitude1,
                 longitude: ^longitude1
               },
               %IPLocation{
                 ip_address: "111.111.111.111",
                 country_code: "NL",
                 country: "Netherlands",
                 city: "Amsterdam",
                 latitude: ^latitude2,
                 longitude: ^longitude2
               },
               %IPLocation{
                 ip_address: "111.111.111.112",
                 country_code: "EE",
                 country: "Estonia",
                 city: "Tallinn",
                 latitude: ^latitude3,
                 longitude: ^longitude3
               }
             ] = Repo.all(from IPLocation, order_by: [asc: :ip_address])

      assert_local_metadata(metadata, 3, 0)
    end

    @tag :tmp_dir
    test "imports local file, discarding rows with empty cells from relevant columns", context do
      Sandbox.checkout(Repo)

      local_csv_path =
        write_csv_data(context, [
          ["111.111.111.110", "BR", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["", "BR", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["111.111.111.112", "", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["111.111.111.113", "BR", "", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["111.111.111.114", "BR", "Brazil", "", "-22.90278", "-43.2075"],
          ["111.111.111.115", "BR", "Brazil", "Rio de Janeiro", "", "-43.2075"],
          ["111.111.111.116", "BR", "Brazil", "Rio de Janeiro", "-22.90278", ""]
        ])

      assert {:ok, metadata} = IPLocationManager.import_from_csv(local_csv_path: local_csv_path)

      latitude1 = Decimal.new("-22.90278")
      longitude1 = Decimal.new("-43.2075")

      assert [
               %IPLocation{
                 ip_address: "111.111.111.110",
                 country_code: "BR",
                 country: "Brazil",
                 city: "Rio de Janeiro",
                 latitude: ^latitude1,
                 longitude: ^longitude1
               }
             ] = Repo.all(from IPLocation, order_by: [asc: :ip_address])

      assert_local_metadata(metadata, 1, 6)
    end

    @tag :tmp_dir
    test "imports local file, discarding rows with invalid values from relevant columns", context do
      Sandbox.checkout(Repo)

      local_csv_path =
        write_csv_data(context, [
          ["111.111.111.110", "BR", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["BR", "BR", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["111.111.111.112", "BRA", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["111.111.111.113", "BR", "       ", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["111.111.111.114", "BR", "Brazil", "        ", "-22.90278", "-43.2075"],
          ["111.111.111.115", "BR", "Brazil", "Rio de Janeiro", "not a number", "-43.2075"],
          ["111.111.111.116", "BR", "Brazil", "Rio de Janeiro", "-22.90278", "not a number"]
        ])

      assert {:ok, metadata} = IPLocationManager.import_from_csv(local_csv_path: local_csv_path)

      latitude1 = Decimal.new("-22.90278")
      longitude1 = Decimal.new("-43.2075")

      assert [
               %IPLocation{
                 ip_address: "111.111.111.110",
                 country_code: "BR",
                 country: "Brazil",
                 city: "Rio de Janeiro",
                 latitude: ^latitude1,
                 longitude: ^longitude1
               }
             ] = Repo.all(from IPLocation, order_by: [asc: :ip_address])

      assert_local_metadata(metadata, 1, 6)
    end

    @tag :tmp_dir
    test "imports local file, discarding rows with without required amount of relevant columns", context do
      Sandbox.checkout(Repo)

      local_csv_path =
        write_csv_data(context, [
          ["111.111.111.110", "BR", "Brazil", "Rio de Janeiro", "-22.90278", "-43.2075"],
          ["111.111.111.111", "BR", "Brazil", "Rio de Janeiro", "-22.90278"],
          ["111.111.111.112", "BR", "Brazil", "Rio de Janeiro"],
          ["111.111.111.113", "BR", "Brazil"],
          ["111.111.111.114", "BR"],
          ["111.111.111.115"],
          []
        ])

      assert {:ok, metadata} = IPLocationManager.import_from_csv(local_csv_path: local_csv_path)

      latitude1 = Decimal.new("-22.90278")
      longitude1 = Decimal.new("-43.2075")

      assert [
               %IPLocation{
                 ip_address: "111.111.111.110",
                 country_code: "BR",
                 country: "Brazil",
                 city: "Rio de Janeiro",
                 latitude: ^latitude1,
                 longitude: ^longitude1
               }
             ] = Repo.all(from IPLocation, order_by: [asc: :ip_address])

      assert_local_metadata(metadata, 1, 6)
    end

    test "fails if CSV does not exist" do
      assert {:error, :failed_to_read_csv} = IPLocationManager.import_from_csv()
    end

    test "fails if download fails" do
      options = [remote_csv_url: "https://ipedatawebsitedoesnotexist.io"]
      assert {:error, :failed_to_download} = IPLocationManager.import_from_csv(options)
    end

    test "fails if download fails (mocked)" do
      options = [remote_csv_url: "https://error", downloader: __MODULE__]
      assert {:error, :failed_to_download} = IPLocationManager.import_from_csv(options)
    end
  end

  defp write_csv_data(context, headers \\ @headers, rows) do
    rows = [headers | rows]
    local_csv_path = Path.join(context.tmp_dir, "ip_locations.csv")
    File.write!(local_csv_path, NimbleCSV.RFC4180.dump_to_iodata(rows))
    local_csv_path
  end

  defp assert_local_metadata(metadata, inserted_amount, discarded_amount) do
    assert %{
             started_at: %NaiveDateTime{},
             downloaded_at: nil,
             finished_at: %NaiveDateTime{},
             inserted_amount: ^inserted_amount,
             discarded_amount: ^discarded_amount
           } = metadata
  end

  defp assert_remote_metadata(metadata, inserted_amount, discarded_amount) do
    assert %{
             started_at: %NaiveDateTime{},
             downloaded_at: %NaiveDateTime{},
             finished_at: %NaiveDateTime{},
             inserted_amount: ^inserted_amount,
             discarded_amount: ^discarded_amount
           } = metadata
  end

  @spec download(String.t(), String.t()) :: :ok | :error
  def download("https://ok", _path), do: :ok
  def download("https://error", _path), do: :error
end
