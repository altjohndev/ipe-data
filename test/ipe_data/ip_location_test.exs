defmodule IpeData.IPLocationTest do
  @moduledoc """
  Test cases related to `IpeData.IPLocation`
  """

  use ExUnit.Case, async: true

  alias Ecto.Changeset
  alias IpeData.IPLocation
  alias IpeData.Test.Fake

  describe "create_changeset/1" do
    test "generates changeset for valid params" do
      params = Fake.schema_params(IPLocation)
      assert %Changeset{valid?: true} = IPLocation.create_changeset(params)
    end

    test "generates changeset, sanitizing ip_address" do
      params = Fake.schema_params(IPLocation, ip_address: "    111.222.10.1    ")
      assert %Changeset{valid?: true} = changeset = IPLocation.create_changeset(params)
      assert Changeset.fetch_change!(changeset, :ip_address) == "111.222.10.1"
    end

    test "generates changeset, sanitizing country_code" do
      params = Fake.schema_params(IPLocation, country_code: "    ee    ")
      assert %Changeset{valid?: true} = changeset = IPLocation.create_changeset(params)
      assert Changeset.fetch_change!(changeset, :country_code) == "EE"
    end

    test "generates changeset, sanitizing country" do
      params = Fake.schema_params(IPLocation, country: " United   States of    America   ")
      assert %Changeset{valid?: true} = changeset = IPLocation.create_changeset(params)
      assert Changeset.fetch_change!(changeset, :country) == "United States of America"
    end

    test "generates changeset, sanitizing city" do
      params = Fake.schema_params(IPLocation, city: " New    York   ")
      assert %Changeset{valid?: true} = changeset = IPLocation.create_changeset(params)
      assert Changeset.fetch_change!(changeset, :city) == "New York"
    end

    test "generates changeset, keeping latitude value if already on modular boundary" do
      params = Fake.schema_params(IPLocation, latitude: "-45")
      assert %Changeset{valid?: true} = changeset = IPLocation.create_changeset(params)
      assert Decimal.equal?(Changeset.fetch_change!(changeset, :latitude), Decimal.new("-45"))
    end

    test "generates changeset, sanitizing latitude modular boundary" do
      params = Fake.schema_params(IPLocation, latitude: "-100.25")
      assert %Changeset{valid?: true} = changeset = IPLocation.create_changeset(params)
      assert Decimal.equal?(Changeset.fetch_change!(changeset, :latitude), Decimal.new("79.75"))
    end

    test "generates changeset, keeping longitude value if already on modular boundary" do
      params = Fake.schema_params(IPLocation, longitude: "135")
      assert %Changeset{valid?: true} = changeset = IPLocation.create_changeset(params)
      assert Decimal.equal?(Changeset.fetch_change!(changeset, :longitude), Decimal.new("135"))
    end

    test "generates changeset, sanitizing longitude modular boundary" do
      params = Fake.schema_params(IPLocation, longitude: "190.33")
      assert %Changeset{valid?: true} = changeset = IPLocation.create_changeset(params)
      assert Decimal.equal?(Changeset.fetch_change!(changeset, :longitude), Decimal.new("-169.67"))
    end

    test "generates invalid changeset on invalid ip_address" do
      params = Fake.schema_params(IPLocation, ip_address: "300.0.0.1")
      assert %Changeset{valid?: false, errors: [ip_address: _error]} = IPLocation.create_changeset(params)
    end

    test "generates invalid changeset on invalid country_code" do
      params = Fake.schema_params(IPLocation, country_code: "ABC")
      assert %Changeset{valid?: false, errors: [country_code: _error]} = IPLocation.create_changeset(params)
    end

    test "generates invalid changeset on invalid country" do
      params = Fake.schema_params(IPLocation, country: "      ")
      assert %Changeset{valid?: false, errors: [country: _error]} = IPLocation.create_changeset(params)
    end

    test "generates invalid changeset on invalid city" do
      params = Fake.schema_params(IPLocation, city: "      ")
      assert %Changeset{valid?: false, errors: [city: _error]} = IPLocation.create_changeset(params)
    end

    test "generates invalid changeset on invalid latitude" do
      params = Fake.schema_params(IPLocation, latitude: "not a number")
      assert %Changeset{valid?: false, errors: [latitude: _error]} = IPLocation.create_changeset(params)
    end

    test "generates invalid changeset on invalid longitude" do
      params = Fake.schema_params(IPLocation, longitude: "not a number")
      assert %Changeset{valid?: false, errors: [longitude: _error]} = IPLocation.create_changeset(params)
    end
  end

  describe "Jason encoding" do
    test "generates JSON" do
      json = ~s|{"ip_address":null,"country_code":null,"country":null,"city":null,"latitude":null,"longitude":null}|
      assert {:ok, ^json} = Jason.encode(%IPLocation{})
    end
  end

  describe "Access behaviour" do
    test "ip_location[field] returns the value" do
      country_code = Fake.country_code()
      ip_location = %IPLocation{country_code: country_code}
      assert ip_location[:country_code] == country_code
    end

    test "ip_location[field] returns nil on invalid field" do
      ip_location = %IPLocation{}
      refute ip_location[:invalid_field]
    end

    test "fetch/2 returns {:ok, value}" do
      country_code = Fake.country_code()
      ip_location = %IPLocation{country_code: country_code}
      assert {:ok, ^country_code} = IPLocation.fetch(ip_location, :country_code)
    end

    test "fetch/2 returns :error on invalid field" do
      assert :error = IPLocation.fetch(%IPLocation{}, :invalid_field)
    end

    test "pop/2 returns {value, ip_location}" do
      country_code = Fake.country_code()
      ip_location = %IPLocation{country_code: country_code}
      assert {^country_code, %IPLocation{country_code: nil}} = IPLocation.pop(ip_location, :country_code)
    end

    test "get_and_update/3 returns {value, ip_location}" do
      country_code = Fake.country_code()
      new_country_code = Fake.country_code()

      ip_location = %IPLocation{country_code: country_code}

      assert {^country_code, %IPLocation{country_code: ^new_country_code}} =
               IPLocation.get_and_update(ip_location, :country_code, fn ^country_code ->
                 {country_code, new_country_code}
               end)
    end

    test "get_and_update/3 returns {value, ip_location} with updated value nil when function returns :pop" do
      country_code = Fake.country_code()

      ip_location = %IPLocation{country_code: country_code}

      assert {^country_code, %IPLocation{country_code: nil}} =
               IPLocation.get_and_update(ip_location, :country_code, fn ^country_code -> :pop end)
    end

    for field <- [:__struct__, :__meta__] do
      @tag field: field
      test "get_and_update/3 returns {value, ip_location} without changing value for :#{field}", %{field: field} do
        ip_location = %IPLocation{}
        value = Map.fetch!(ip_location, field)

        assert {^value, %IPLocation{} = ^ip_location} =
                 IPLocation.get_and_update(ip_location, field, fn ^value -> {value, "updated value"} end)
      end
    end
  end
end
