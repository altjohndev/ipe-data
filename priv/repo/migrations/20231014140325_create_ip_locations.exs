defmodule IpeData.Repo.Migrations.CreateIPLocations do
  @moduledoc """
  Creates "ip_locations" table.
  """

  use Ecto.Migration

  def change do
    create table("ip_locations", primary_key: false) do
      add :ip_address, :string, primary_key: true
      add :country_code, :string, null: false
      add :country, :string, null: false
      add :city, :string, null: false
      add :latitude, :decimal, null: false
      add :longitude, :decimal, null: false
    end
  end
end
