defmodule IpeData.MixProject do
  @moduledoc false

  use Mix.Project

  @source_url "https://gitlab.com/altjohndev/ipe-data"
  @version "0.0.1"

  def project do
    env = Mix.env()

    [
      app: :ipe_data,
      name: "Ipê Data",
      description: "Data management library for Ipê services",
      elixir: "~> 1.15",
      version: @version,
      source_url: @source_url,
      deps: deps(),
      aliases: aliases(),
      preferred_cli_env: preferred_cli_env(),
      elixirc_options: [warnings_as_errors: true],
      elixirc_paths: elixirc_paths(env),
      package: package(),
      docs: docs(),
      test_coverage: test_coverage()
    ]
  end

  def application do
    [
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  defp deps do
    [
      {:credo, ">= 1.0.0", only: [:dev, :test], runtime: false, optional: true},
      {:ecto_sql, ">= 3.0.0"},
      {:ecto, ">= 3.0.0"},
      {:ex_doc, "> 0.0.0", only: [:dev, :test], runtime: false, optional: true},
      {:jason, ">= 1.0.0"},
      {:nimble_csv, ">= 1.0.0"},
      {:postgrex, "> 0.0.0", only: [:dev, :test], optional: true},
      {:styler, "> 0.0.0", only: [:dev, :test], runtime: false, optional: true}
    ]
  end

  defp aliases do
    [
      "ecto.migrate": ["ipe_data.migrate IpeData.Test.Repo"],
      "ecto.reset": ["ecto.drop", "ecto.create", "ecto.migrate"],
      "test.all": ["test.static", "test"],
      "test.ci": ["test.static", "ecto.reset", "test --trace --cover"],
      "test.static": ["format --check-formatted", "credo"]
    ]
  end

  defp preferred_cli_env do
    [
      "ecto.migrate": :test,
      "ecto.reset": :test,
      "test.all": :test,
      "test.ci": :test,
      "test.static": :test,
      test: :test
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_env), do: ["lib"]

  defp package do
    [
      licenses: ["MIT"],
      links: %{gitlab: @source_url}
    ]
  end

  defp docs do
    [
      source_ref: "v#{@version}",
      extras: ["README.md", "LICENSE"],
      main: "readme"
    ]
  end

  defp test_coverage do
    [
      summary: [threshold: 80],
      ignore_modules: [
        ~r/IpeData.Test.*/
      ]
    ]
  end
end
